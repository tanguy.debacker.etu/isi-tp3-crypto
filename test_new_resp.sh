
echo "nom du responsable"
read resp
openssl   aes-256-cbc -d -in usb$resp/key$resp.enc -out ramdisk/key$resp.clear -iv 0
if [[ $resp =~ [A|a][0-9]+ ]] ; then
echo "Mot de passe du B responsable"
openssl   aes-256-cbc -d -in usbB1/keyB1.enc -out ramdisk/keyB1.clear -iv 0

echo -n "$(cat ramdisk/key$resp.clear)" > ramdisk/'fullkey'$resp'B1'.clear
echo -n "$(cat ramdisk/keyB1.clear)" >> ramdisk/'fullkey'$resp'B1'.clear
openssl  aes-256-cbc  -d -in 'Mkey'$resp'B1'.enc  -out ramdisk/Mkey.clear  -iv 0 -kfile ramdisk/'fullkey'$resp'B1'.clear
fi
if [[ $resp =~ [B|b][0-9]+ ]] ; then
echo "Mot de passe du A responsable"
openssl   aes-256-cbc -d -in usbA1/keyA1.enc -out ramdisk/keyA1.clear -iv 0
openssl   aes-256-cbc -d -in usb$resp/key$resp.enc -out ramdisk/key$resp.clear -iv 0
echo -n "$(cat ramdisk/keyA1.clear)" > ramdisk/fullkeyA1$resp.clear
echo -n "$(cat ramdisk/key$resp.clear)" >> ramdisk/fullkeyA1$resp.clear
openssl  aes-256-cbc  -d  -in MkeyA1$resp.enc  -out ramdisk/Mkey.clear  -iv 0 -kfile ramdisk/fullkeyA1$resp.clear
fi
openssl aes-256-cbc -d -in info.enc -out ramdisk/data -iv 0 -kfile ramdisk/Mkey.clear

cat ramdisk/data
rm ramdisk/*
