#!/bin/bash
# 1) Chaque responsable insère sa clé usb, elles sont dispo sous usb1/ et usb2/
# 2) On génère une clé aléatoirement pour chaque responsable
#touch info 
#cat 0000 0000 0000 0000 toto > info
dd if=/dev/random bs=32 count=1 > ramdisk/keyA1.clear
dd if=/dev/random bs=32 count=1 > ramdisk/keyB1.clear
dd if=/dev/random bs=32 count=1 > ramdisk/Mkey.clear
echo -n "$(cat ramdisk/keyA1.clear)" > ramdisk/fullkeyA1B1.clear
echo -n "$(cat ramdisk/keyB1.clear)" >> ramdisk/fullkeyA1B1.clear
mkdir usbB1 usbA1

# 3)CRYPTER MA MASTER key
openssl enc -aes-256-cbc -in ramdisk/Mkey.clear -out MkeyA1B1.enc -iv 0 -kfile ramdisk/fullkeyA1B1.clear
# 4) crypter data avec la Mkey
openssl enc -aes-256-cbc -in info -out info.enc -iv 0 -kfile ramdisk/Mkey.clear
# 5) Les responsables choisissent un mot de passe pour protéger leur clé. Leur clé protégée est copiée sur leur clé usb 
openssl enc -aes-256-cbc -in ramdisk/keyA1.clear -out usbA1/keyA1.enc -iv 0
openssl enc -aes-256-cbc -in ramdisk/keyB1.clear -out usbB1/keyB1.enc -iv 0
# 6) On supprime les clés en clair
rm ramdisk/*
rm info 

