#!/bin/bash

# Les responsables tapent leur mot de passe
echo "Mot de passe du A responsable"
openssl   aes-256-cbc -d -in usbA1/keyA1.enc -out ramdisk/keyA1.clear -iv 0
echo "Mot de passe du B responsable"
openssl   aes-256-cbc -d -in usbB1/keyB1.enc -out ramdisk/keyB1.clear -iv 0

# Les deux clés sont concaténées pour créer la clé finale
echo -n "$(cat ramdisk/keyA1.clear)" > ramdisk/fullkeyA1B1.clear
echo -n "$(cat ramdisk/keyB1.clear)" >> ramdisk/fullkeyA1B1.clear


openssl aes-256-cbc -d -in MkeyA1B1.enc -out ramdisk/Mkey.clear -kfile ramdisk/fullkeyA1B1.clear -iv 0

# Maintenant la clé est en clair dans la RAM, on peut l'utiliser pour accéder au fichier de carte bancaires
openssl aes-256-cbc -d -in info.enc -out ramdisk/data -kfile ramdisk/Mkey.clear -iv 0
#manipulation du data:
while true ; do
echo "Bonjour Monsieur le responsable "
echo "1-recherche ** 2-ajoute ** 3- supprimer ** 4-revocation ** 5-add Responsable ** 0-exit "
read -r choix 
case $choix in
0)
openssl enc -aes-256-cbc -in ramdisk/data -out info.enc -iv 0 -kfile ramdisk/Mkey.clear
rm ramdisk/*
break
;;
1 )
echo "l'information que vous chercher" 
read -r info
grep -i $info ramdisk/data
;;
2) 
echo "entrer une nouvelle donnnée "
read data
echo $data >> ramdisk/data 
;;
3)
echo "supprimer une donnée"
read  data
rminfo=$(grep -i $data ramdisk/data) 
echo '/'$rminfo'/d'
sed -i "/$rminfo/d" ramdisk/data 
;;
4)
echo "nom du reponsable a supprimer"
read resp
rm Mkey*$resp.enc
rm Mkey$resp*.enc
rm -r usb$resp
;;
5)
echo " de type ? A OR B and number "
read resp

dd if=/dev/random bs=32 count=1 > ramdisk/key$resp.clear
mkdir usb$resp
openssl enc -aes-256-cbc -in ramdisk/key$resp.clear -out usb$resp/key$resp.enc -iv 0

if [[ $resp =~ [A|a][0-9]+ ]] ; then
echo "Mot de passe du B responsable"
openssl   aes-256-cbc -d -in usbB1/keyB1.enc -out ramdisk/keyB1.clear -iv 0
echo -n "$(cat ramdisk/key$resp.clear)" > ramdisk/'fullkey'$resp'B1'.clear
echo -n "$(cat ramdisk/keyB1.clear)" >> ramdisk/'fullkey'$resp'B1'.clear
openssl enc -aes-256-cbc -in ramdisk/Mkey.clear -out 'Mkey'$resp'B1'.enc -iv 0 -kfile ramdisk/'fullkey'$resp'B1'.clear
fi

if [[ $resp =~ [B|b][0-9]+ ]] ; then
echo "Mot de passe du A responsable"
openssl   aes-256-cbc -d -in usbA1/keyA1.enc -out ramdisk/keyA1.clear -iv 0
echo -n "$(cat ramdisk/keyA1.clear)" > ramdisk/fullkeyA1$resp.clear
echo -n "$(cat ramdisk/key$resp.clear)" >> ramdisk/fullkeyA1$resp.clear
openssl enc -aes-256-cbc -in ramdisk/Mkey.clear -out MkeyA1$resp.enc -iv 0 -kfile ramdisk/fullkeyA1$resp.clear
fi
;;
*)
echo "Unknown response, enter a number 0-5 or type 'exit' to quit" 
;;
esac
done 
